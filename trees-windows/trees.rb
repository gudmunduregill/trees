def generateTree(numberOfVertices)
  def hasCycle(graph, numberOfNodes)
    nodes = graph.keys
    nodes.each do |f|
      if graph[f].length <= 1 # graph[f] is a leaf
        nodes.each do |key| # remove leaf from graph
          graph[key].reject! { |x| x == f}
        end
        graph[f] = []
      end
    end
    newNumberOfNodes = graph.flatten.flatten.count - nodes.count
    if newNumberOfNodes == 0
      return false
    elsif newNumberOfNodes == numberOfNodes # We are in a cycle
      return true
    else
      return hasCycle(graph, newNumberOfNodes)
    end
  end

  def willBecomeCycle?(cloneOfGraph, v1, v2) # will connecting these two create a cycle?
    both = false
    cloneOfGraph[v1].push v2
    cloneOfGraph[v2].push v1
    cloneOfGraph[v1].each do |f|
      cloneOfGraph[v2].each do |g|
        if f == g
          return true
        end
      end
    end
    return hasCycle(cloneOfGraph, cloneOfGraph.flatten.flatten.count)
  end

  def alreadyConnected?(graph, s1, s2)
    return (graph[s1].include? s2 or graph[s2].include? s1)
  end

  def isTreeReady(graph)
    graph.flatten.each do |f| 
      if f.empty?
        return false
      end
    end
    return true
  end

  def constructGraph(graph, vertices)
    if isTreeReady(graph)
      #puts "Tree is ready: "
      return graph
    else # Our tree is not ready
      firstSymbol = vertices[rand(0..8)]
      otherSymbol = vertices[rand(0..8)]
      if firstSymbol == otherSymbol
        constructGraph(graph, vertices)
      else # We have two distinctly selected symbols (for vertices)
        firstVertex = graph[firstSymbol]
        otherVertex = graph[otherSymbol]
        #puts "firstVertex, #{firstSymbol}: #{firstVertex}"
        #puts "otherVertex, #{otherSymbol}: #{otherVertex}"
        cloneOfGraph = graph.clone
        if (!firstVertex.include? otherSymbol and !otherVertex.include? firstSymbol)
          firstVertex.push(otherSymbol)
          otherVertex.push(firstSymbol)
          constructGraph(graph, vertices)
        else
          constructGraph(graph, vertices)
        end
      end
    end
  end

  while true
    graph = {
      :a => [], :b => [], :c => [],
      :d => [], :e => [], :f => [],
      :g => [], :h => [], :i => []
    }
    tree = constructGraph(graph, graph.keys)
    if tree.values.flatten.count / 2 == 8
      originalTree = "#{tree.dup}"
      if !hasCycle(tree, tree.keys.count)
        #puts "EDGES: #{eval(originalTree).values.flatten.count / 2 }"
        #puts originalTree
        originalTree = eval(originalTree)
        return originalTree
      end
    end
  end
end
